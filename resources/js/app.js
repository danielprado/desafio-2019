
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./jquery.min');
require('./jquery.poptrox.min');
require('./jquery.scrolly.min');
require('./jquery.scrollex.min');
require('./browser.min');
require('./breakpoints.min');
require('./util');
require('./main');
