<!-- Work -->
<section id="work" class="main style3 primary">
    <div class="content">
        <header>
            <h2>Galería</h2>
            <p>Te invitamos a realizar actividad física por lo menos 15 minutos durante el día.</p>
        </header>

        <!-- Gallery  -->
        <div class="gallery">
            <article class="from-left">
                <a href="{{ asset('public/img/fulls/1.jpg') }}" class="image fit"><img src="{{ asset('public/img/thumbs/1.png') }}" title="1" alt="1" /></a>
            </article>
            <article class="from-left">
                <a href="{{ asset('public/img/fulls/2.jpg') }}" class="image fit"><img src="{{ asset('public/img/thumbs/2.png') }}" title="2" alt="2" /></a>
            </article>
            <article class="from-right">
                <a href="{{ asset('public/img/fulls/3.jpg') }}" class="image fit"><img src="{{ asset('public/img/thumbs/3.png') }}" title="3" alt="3" /></a>
            </article>
            <article class="from-right">
                <a href="{{ asset('public/img/fulls/4.jpg') }}" class="image fit"><img src="{{ asset('public/img/thumbs/4.png') }}" title="4" alt="4" /></a>
            </article>
            <article class="from-right">
                <a href="{{ asset('public/img/fulls/5.jpg') }}" class="image fit"><img src="{{ asset('public/img/thumbs/5.png') }}" title="5" alt="5" /></a>
            </article>
            <article class="from-right">
                <a href="{{ asset('public/img/fulls/6.jpg') }}" class="image fit"><img src="{{ asset('public/img/thumbs/6.png') }}" title="6" alt="6" /></a>
            </article>
        </div>

    </div>
</section>