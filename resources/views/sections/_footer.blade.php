<!-- Footer -->
<footer id="footer">

    <!-- Icons -->
    <ul class="icons">
        <li><a href="javascript:;" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="javascript:;" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
    </ul>

    <!-- Menu -->
    <ul class="menu">
        <li>{{ \Carbon\Carbon::now()->year }} &copy; IDRD</li>
        <li>Hecho con <i class="icon fa-heart"></i> para una web mejor.</li>
    </ul>

</footer>