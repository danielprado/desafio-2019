<!-- Contact -->
<section id="contact" class="main style3 secondary">
    <div class="content">
        <header>
            <h2>Inscripción</h2>
            <p>Formulario de inscripción día del desafío 2019.</p>
        </header>
        <div class="box">
            {{--
            <h2>Gracias por haber participado</h2>
            --}}
            <form method="POST" action="{{ route('insertar.participante') }}" id="form_gen">
                <div class="fields">
                    <div class="field half">
                        <select  required name="actividad" id="actividad" class="form-control" data-readonly="tipo_actividad" data-readonly-value="19">
                            <option value="">Actividad realizada</option>
                            @foreach ($actividades as $actividad)
                                <option value="{{ $actividad->id_actividad}}">{{ $actividad->actividad}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="field half">
                        <input type="text" class="form-control" id="tipo_actividad" name="tipo_actividad" readonly placeholder="¿Cuál?">
                    </div>
                    <div class="field half">
                        <input required type='text' class="form-control timepicker" id="hora" name="hora" placeholder="Hora" />
                    </div>
                    <div class="field half">
                        <input required type="text" class="form-control" id="nombre_coordinador" name="nombre_coordinador" placeholder="Coordinador de la actividad">
                    </div>
                    <div class="field half">
                        <input required type="tel" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
                    </div>
                    <div class="field half">
                        <input required type="text" class="form-control" id="entidad" name="entidad" placeholder="Entidad/Grupo/Persona/Comunidad">
                    </div>
                    <div class="field half">
                        <select  required name="sector" id="sector" class="form-control" >
                            <option value="">Sector</option>
                            <option value="1">Público</option>
                            <option value="2">Privado</option>
                        </select>
                    </div>
                    <div class="field half">
                        <select  required name="localidad" id="localidad" class="form-control" >
                            <option value="">Localidad</option>
                            @foreach ($localidades as $localidad)
                                <option value="{{ $localidad->id_localidad }}">{{ $localidad->localidad}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="field">
                        <input required type="text" required class="form-control" id="direccion" name="direccion" placeholder="Dirección">
                    </div>
                    <div class="field">
                        <p>Cantidad de participantes: <span class="counter" id="participantes_total">0</span></p>
                    </div>
                    <div class="field half">
                        <input type="number" required min="0"  class="form-control" id="hombres" name="hombres" placeholder="Cantidad Participantes Hombres">
                    </div>
                    <div class="field half">
                        <input type="number" required min="0"  class="form-control" id="mujeres" name="mujeres" placeholder="Cantidad Participantes Mujeres">
                    </div>
                    <div class="field">
                        <textarea name="observaciones" placeholder="Observaciones" rows="3"></textarea>
                    </div>
                    <div class="field">
                        <p>Términos de inscripción</p>
                        <small>
                            Mediante el diligenciamiento de este formulario, autorizo al Instituto Distrital de Recreación y Deporte – IDRD,
                            así como a cualquier dependencia aliada o filial del Instituto, a realizar el tratamiento sobre mis datos personales,
                            conforme a la ley 1581 de 2012, con la finalidad de ser utilizados para realizar registros de participación en el Día del Desafío {{ \Carbon\Carbon::now()->year }}
                            Manifiesto que he decidido participar voluntariamente en la actividad que ofrece el Instituto Distrital de Recreación y Deporte – IDRD.
                        </small>
                    </div>

                </div>
                <ul class="actions special">
                    <li><input type="submit" value="Registrarme" /></li>
                </ul>
            </form>
            
        </div>
    </div>
</section>