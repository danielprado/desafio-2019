<!-- Intro -->
<section id="message" class="main style1 dark fullscreen">
    <div class="content">
        <header>
            <h2>Día del Desafío {{ \Carbon\Carbon::now()->year }}</h2>
        </header>
        <p>{{ isset( $message ) ? $message : '' }}</p>
        <footer>
            <a href="{{ route('home') }}" class="button style2">Ir a Inicio</a>
        </footer>
    </div>
</section>