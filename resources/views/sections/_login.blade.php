
<!-- One -->
<section id="one" class="main style2 right dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>Iniciar Sesión</h2>
        </header>
        <div class="box">
            <form method="POST" action="{{ route('loguear') }}" id="form_gen">
                <div class="fields">
                    <div class="field">
                        <input required type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario">
                    </div>
                    <div class="field">
                        <input required type='password' class="form-control" id="password" name="password" placeholder="Contraseña" />
                    </div>
                </div>
                <ul class="actions special">
                    <li><input type="submit" value="Iniciar Sesión" /></li>
                </ul>
            </form>
        </div>
    </div>
</section>