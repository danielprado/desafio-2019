<!-- One -->
<section id="one" class="main style2 right dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>Introducción</h2>
        </header>
        <p>Los datos de inactividad física, comportamientos sedentarios y exceso de peso en
            la población de Bogotá han ido en aumento. Es así como la ENSIN 2010 (ICBF), revela que la prevalencia
            de tiempo dedicado a ver televisión o jugar con videojuegos en niños y niñas, entre los 5 y 12 años
            en Bogotá (mayor a 2 horas) es de 69.9%, presentando uno de los índices más altos en el país,
            mientras que los adolescentes, entre 13 y 17 años, presentaron una prevalencia en Bogotá de 76.6%.
        </p>
        <p>
            En lo que se refiere a los adultos, entre 18 a 64 años, también en la capital, un 37,2% presentó sobrepeso
            y un 14,1% obesidad. Todo esto refleja que estamos ante un 51,3% de problemas de exceso de peso, sobrepasando
            el 48,5% con respecto al 2005. Así mismo, la prevalencia de obesidad abdominal por circunferencia de cintura
            en hombres y mujeres para el año 2010 en Bogotá, muestra un incremento de 10% para los hombres (43,4%)
            y en mujeres del 12,4% (60,7%).
        </p>
    </div>
    <a href="#two" class="button style2 down anchored">Next</a>
</section>