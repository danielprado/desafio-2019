<!-- Intro -->
<section id="intro" class="main style1 dark fullscreen">
    <div class="hero" style="
    background-color: #563d7c;
    content: '';
    display: block;
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    opacity: 0.4;
"></div>
    <div class="content">
        <header>
            <h2>Día del Desafío {{ \Carbon\Carbon::now()->year }}</h2>
        </header>
        <p><b>Evento que llevara a cabo el</b></p>
        <h1 class="kreep">
            <a href="#contact" class="button style2"><b>27 de mayo</b></a>
        </h1>
        <footer>
            <a href="#one" class="button style2 down">More</a>
        </footer>
    </div>
</section>