<!-- Two -->
<section id="two" class="main style2 left dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>Descripción</h2>
        </header>
        <p>El Día del desafío es una actividad  dirigida a todos los habitantes de Bogotá, pretende movilizar el mayor
            numero de participantes realizando actividades físicas, recreativas  deportivas  o cotidianas que impliquen
            movimiento   durante 15 minutos  continuos.
        </p>
        <p>
            Se compite con otra ciudad del mundo.
        </p>
        <p>
            Las actividades este año pretenden concientizar a la ciudadanía sobre la importancia de la practica constante
            de actividad física, como estrategia contra las enfermedades ocasionadas por el sedentarismo y sobre peso.
        </p>
    </div>
    <a href="#work" class="button style2 down anchored">Next</a>
</section>