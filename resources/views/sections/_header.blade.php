<header id="header">
    <h1>IDRD {{ \Carbon\Carbon::now()->year }}</h1>
    <nav>
        @if( Route::currentRouteName() != 'home' && Route::currentRouteName() != 'loguear')
            <ul>
                <li><a href="{{ route('home') }}#intro">Inicio</a></li>
                <li><a href="{{ route('home') }}#one">Introducción</a></li>
                <li><a href="{{ route('home') }}#two">Descripción</a></li>
                <li><a href="{{ route('home') }}#work">Galería</a></li>
                <li><a href="{{ route('home') }}#contact" class="contact">Inscripciones</a></li>
            </ul>
        @elseif( Route::currentRouteName() == 'loguear' && isset($_SESSION['id_usuario']))
            <ul>
                <li><a href="{{ route('logout') }}">Cerrar Sesion</a></li>
            </ul>
        @else
            <ul>
                <li><a href="#intro">Inicio</a></li>
                <li><a href="#one">Introducción</a></li>
                <li><a href="#two">Descripción</a></li>
                <li><a href="#work">Galería</a></li>
                <li><a href="#contact" class="contact">Inscripciones</a></li>
            </ul>
        @endif
    </nav>
</header>