@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.validation_failed'))
@section('code', '422')
@section('message', trans('validation.handler.validation_failed'))
@section('image')
    <div style="background-image: url({{ asset('public/svg/404.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
