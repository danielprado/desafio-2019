@extends('master')

@section('content')
    <section class="main style3 secondary">
        <div class="content">
            <header>
                <h2>Datos Registrados Desafío {{ \Carbon\Carbon::now()->year }}</h2>
                @php

                $h = isset( $counters[0]['count_h'] ) ? (int) $counters[0]['count_h'] : 0;
                $m = isset( $counters[0]['count_m'] ) ? (int) $counters[0]['count_m'] : 0;

                $total = $h + $m;

                @endphp
                <p>Hombres: {{ $h }} / Mujeres: {{ $m }} / Total: {{ $total }}</p>
            </header>
            <div class="box">
                <div class="col-md-12" id="div-tabla" >

                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/datatables.min.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/datatables.min.js"></script>
    <script>
      $(document).ready(function() {
        $.ajax({url: "{{ route('datatable') }}",type:  'post', success:  function (response) {
            $('#div-tabla').html(response);
          },complete: function(){
            $('#lista').DataTable({
              "language": {"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"},
              responsive: true,
              dom: 'Bfrtip',
              buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
              ]
            });
          }
        });
      });
    </script>
@endpush
