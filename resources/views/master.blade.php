<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="{{ asset('public/favicon.ico') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'IDRD - Desafío 2020') }}</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

        <link rel="stylesheet" href="{{ asset('public/css/main.css') }}?v={{ sha1_file( public_path('css/main.css') ) }}" />
        <noscript><link rel="stylesheet" href="{{ asset('public/css/noscript.css') }}" /></noscript>

    </head>
    <body class="is-preload">

        @include('sections._header')
        @yield('content')
        @include('sections._footer')
        <!-- Scripts -->
        <script src="{{ asset('public/js/app.js') }}?v={{ sha1_file( public_path('js/app.js') ) }}" type="text/javascript"></script>
        @stack('scripts')
    </body>
</html>