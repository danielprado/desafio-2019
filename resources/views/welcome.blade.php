@extends('master')

@section('content')
    @include('sections._intro')
    @include('sections._one')
    @include('sections._two')
    @include('sections._work')
    @include('sections._form')
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.11.6/dist/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script>
      $(document).ready(function() {

          $('#hora').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '6',
            maxTime: '6:00pm',
            startTime: '6:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
          });

        /*
          let updateCount = function() {
            let counters = $(".counter");
            let countersQuantity = counters.length;
            let counter = [];

            for (let i = 0; i < countersQuantity; i++) {
              counter[i] = parseInt(counters[i].innerHTML);
            }

            let count = function(start, value, id) {
              let localStart = start;
              setInterval(function() {
                if (localStart < value) {
                  localStart++;
                  counters[id].innerHTML = localStart;
                }
              }, 40);
            };

            for (j = 0; j < countersQuantity; j++) {
              count(0, counter[j], j);
            }
          };
          updateCount();
        */

        $('input[type=number]').on('keyup', function () {
          let num = parseInt( $(this).val() );
          if (isNaN(num)) {
            return $(this).val('');
          }
          return $(this).val( num );
        });

        $('input[type=tel]').on('keyup', function () {
          let num = parseInt( $(this).val() );
          if (isNaN(num)) {
            return $(this).val('');
          }
          return $(this).val( num );
        });

        $('input[name=hombres]').change(function() {
          $('#participantes_total').text( parseInt( $(this).val() ) + parseInt( $('#mujeres').val() || 0 ) );
          updateCount();
        });

        $('input[name=mujeres]').change(function() {
          $('#participantes_total').text( parseInt( $(this).val() ) + parseInt( $('#hombres').val() || 0 ) );
          updateCount();
        });

        $('select[data-readonly]').on('change', function(e){
          let input = $(this).data('readonly');
          let readonly_value = $(this).data('readonly-value');
          let data = $('*[name="'+input+'"]');
          if(readonly_value != $(this).val()) {
            data.attr('readonly', 'readonly');
            data.val('');
          } else {
            data.removeAttr('readonly');
            data.val('');
          }
        });

      });
    </script>
@endpush