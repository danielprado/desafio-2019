@extends('master')                              

@section('content') 


        
  <div id="top" class="page-header section-dark" style="background-image: url('{{ asset('public/images/auditorium.jpg') }}')">
    <div class="filter"></div>
    <div class="content-center">
        <div class="container">
            <div class="title-brand">
                <h3 class="presentation-title">
                    {{$error}}
                </h3>
            </div>

            <h1 class="presentation-subtitle text-center">
                5° CONGRESO NACIONAL Y 2° INTERNACIONAL DE ACTIVIDAD FÍSICA<br>
               
               <img src="{{ asset('public/images/idrdblanco.png') }}" heigth="30%"  width="30%" alt="">
            </h1>
        </div>
    </div>
    <div class="moving-clouds" style="background-image: url('{{ asset('public/images/clouds.png') }}'); ">

    </div>

    <h6 class="category category-absolute">
        <a href="#description" class="text-white-50 top-menu">
            <b>
                Desliza hacia abajo <br> <i class="nc-icon nc-minimal-down"></i>
            </b>
        </a>
    </h6>

</div>      
@stop


