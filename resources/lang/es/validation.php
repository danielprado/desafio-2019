<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => ':attribute debe ser una fecha posterior a :date.',
    'after_or_equal'       => ':attribute debe ser una fecha posterior o igual a :date.',
    'alpha'                => ':attribute sólo debe contener letras.',
    'alpha_dash'           => ':attribute sólo debe contener letras, números y guiones.',
    'alpha_num'            => ':attribute sólo debe contener letras y números.',
    'array'                => ':attribute debe ser un conjunto.',
    'before'               => ':attribute debe ser una fecha anterior a :date.',
    'before_or_equal'      => ':attribute debe ser una fecha anterior o igual a :date.',
    'between'              => [
        'numeric' => ':attribute tiene que estar entre :min - :max.',
        'file'    => ':attribute debe pesar entre :min - :max kilobytes.',
        'string'  => ':attribute tiene que tener entre :min - :max caracteres.',
        'array'   => ':attribute tiene que tener entre :min - :max ítems.',
    ],
    'boolean'              => 'El campo :attribute debe tener un valor verdadero o falso.',
    'confirmed'            => 'La confirmación de :attribute no coincide.',
    'date'                 => ':attribute no es una fecha válida.',
    'date_equals'          => ':attribute debe ser una fecha igual a :date.',
    'date_format'          => ':attribute no corresponde al formato :format.',
    'different'            => ':attribute y :other deben ser diferentes.',
    'digits'               => ':attribute debe tener :digits dígitos.',
    'digits_between'       => ':attribute debe tener entre :min y :max dígitos.',
    'dimensions'           => 'Las dimensiones de la imagen :attribute no son válidas.',
    'distinct'             => 'El campo :attribute contiene un valor duplicado.',
    'email'                => ':attribute no es un correo válido',
    'exists'               => ':attribute es inválido.',
    'file'                 => 'El campo :attribute debe ser un archivo.',
    'filled'               => 'El campo :attribute es obligatorio.',
    'gt'                   => [
        'numeric' => 'El campo :attribute debe ser mayor que :value.',
        'file'    => 'El campo :attribute debe tener más de :value kilobytes.',
        'string'  => 'El campo :attribute debe tener más de :value caracteres.',
        'array'   => 'El campo :attribute debe tener más de :value elementos.',
    ],
    'gte'                  => [
        'numeric' => 'El campo :attribute debe ser como mínimo :value.',
        'file'    => 'El campo :attribute debe tener como mínimo :value kilobytes.',
        'string'  => 'El campo :attribute debe tener como mínimo :value caracteres.',
        'array'   => 'El campo :attribute debe tener como mínimo :value elementos.',
    ],
    'image'                => ':attribute debe ser una imagen.',
    'in'                   => ':attribute es inválido.',
    'in_array'             => 'El campo :attribute no existe en :other.',
    'integer'              => ':attribute debe ser un número entero.',
    'ip'                   => ':attribute debe ser una dirección IP válida.',
    'ipv4'                 => ':attribute debe ser un dirección IPv4 válida',
    'ipv6'                 => ':attribute debe ser un dirección IPv6 válida.',
    'json'                 => 'El campo :attribute debe tener una cadena JSON válida.',
    'lt'                   => [
        'numeric' => 'El campo :attribute debe ser menor que :value.',
        'file'    => 'El campo :attribute debe tener menos de :value kilobytes.',
        'string'  => 'El campo :attribute debe tener menos de :value caracteres.',
        'array'   => 'El campo :attribute debe tener menos de :value elementos.',
    ],
    'lte'                  => [
        'numeric' => 'El campo :attribute debe ser como máximo :value.',
        'file'    => 'El campo :attribute debe tener como máximo :value kilobytes.',
        'string'  => 'El campo :attribute debe tener como máximo :value caracteres.',
        'array'   => 'El campo :attribute debe tener como máximo :value elementos.',
    ],
    'max'                  => [
        'numeric' => ':attribute no debe ser mayor a :max.',
        'file'    => ':attribute no debe ser mayor que :max kilobytes.',
        'string'  => ':attribute no debe ser mayor que :max caracteres.',
        'array'   => ':attribute no debe tener más de :max elementos.',
    ],
    'mimes'                => ':attribute debe ser un archivo con formato: :values.',
    'mimetypes'            => ':attribute debe ser un archivo con formato: :values.',
    'min'                  => [
        'numeric' => 'El tamaño de :attribute debe ser de al menos :min.',
        'file'    => 'El tamaño de :attribute debe ser de al menos :min kilobytes.',
        'string'  => ':attribute debe contener al menos :min caracteres.',
        'array'   => ':attribute debe tener al menos :min elementos.',
    ],
    'not_in'               => ':attribute es inválido.',
    'not_regex'            => 'El formato del campo :attribute no es válido.',
    'numeric'              => ':attribute debe ser numérico.',
    'present'              => 'El campo :attribute debe estar presente.',
    'regex'                => 'El formato de :attribute es inválido.',
    'required'             => 'El campo :attribute es obligatorio.',
    'required_if'          => 'El campo :attribute es obligatorio cuando :other es :value.',
    'required_unless'      => 'El campo :attribute es obligatorio a menos que :other esté en :values.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de :values estén presentes.',
    'same'                 => ':attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El tamaño de :attribute debe ser :size.',
        'file'    => 'El tamaño de :attribute debe ser :size kilobytes.',
        'string'  => ':attribute debe contener :size caracteres.',
        'array'   => ':attribute debe contener :size elementos.',
    ],
    'starts_with'          => 'El campo :attribute debe comenzar con uno de los siguientes valores: :values',
    'string'               => 'El campo :attribute debe ser una cadena de caracteres.',
    'timezone'             => 'El :attribute debe ser una zona válida.',
    'unique'               => 'El campo :attribute ya ha sido registrado.',
    'uploaded'             => 'Subir :attribute ha fallado.',
    'url'                  => 'El formato :attribute es inválido.',
    'uuid'                 => 'El campo :attribute debe ser un UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password' => [
            'min' => 'La :attribute debe contener más de :min caracteres',
        ],
        'email'    => [
            'unique' => 'El :attribute ya ha sido registrado.',
        ],
    ],

    /*
   |--------------------------------------------------------------------------
   | Error Message Language Lines
   |--------------------------------------------------------------------------
   |
   | The following language lines are used by the Api Responder.
   | When it generates error responses, it will search the messages array
   | below for any key matching the given error code for the response.
   |
   */
    'handler'   =>  [
        'resource_not_found'        => 'No existe ninguna instacia para el valor especificado.',
        'resource_not_found_url'    => 'La URL solicitada no existe.',
        'unauthenticated'           => 'No estás autenticado para esta solicitud.',
        'unauthorized'              => 'No estás autorizado para esta solicitud.',
        'relation_not_found'        => 'La relación solicitada no existe.',
        'column_not_found'          => 'La columna solicitada no existe.',
        'relation_not_delete'       => 'El recurso no puede ser eliminado, contiene información asociada.',
        'validation_failed'         => 'Por favor completa todos los datos del formulario.',
        'syntax_error'              => 'la sintaxis de la entidad de solicitud no es correcta.',
        'method_allow'              => 'El método especificado no es válido.',
        'unexpected_failure'        => 'Error inesperado, inténtalo de nuevo en un momento.',
        'conflict'                  => 'La solicitud no se pudo procesar debido a un conflicto.',
        'service_unavailable'       => 'El servicio no está disponible, vuelve a intentarlo en un momento.',
        'token_mismatch'            => 'El token no coincide con el solicitado, por favor intenta recargar la página.',
        'max_attempts'              => 'Demasiados intentos, por favor ralentice la solicitud e intente de nuevo en :sec segundos.',
        'connection_refused'        => ':db ha rechazado la conexión.',
        'go_home'                   => 'Ir a inicio',
        'success'                   => 'Los datos se han procesado satisfactoriamente.',
        'park'                      => 'Este parque se ha programade :num veces este mes.',
        'attention'                 => 'Para esta semana ya existe una programación de atención ciudadana.',
        'exist'                     => 'Para este día ya existe una programación creada.',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name'                  => 'nombre',
        'username'              => 'usuario',
        'email'                 => 'correo electrónico',
        'first_name'            => 'nombre',
        'last_name'             => 'apellido',
        'password'              => 'contraseña',
        'password_confirmation' => 'confirmación de la contraseña',
        'city'                  => 'ciudad',
        'country'               => 'país',
        'address'               => 'dirección',
        'phone'                 => 'teléfono',
        'mobile'                => 'móvil',
        'age'                   => 'edad',
        'sex'                   => 'sexo',
        'gender'                => 'género',
        'year'                  => 'año',
        'month'                 => 'mes',
        'day'                   => 'día',
        'hour'                  => 'hora',
        'minute'                => 'minuto',
        'second'                => 'segundo',
        'title'                 => 'título',
        'content'               => 'contenido',
        'body'                  => 'contenido',
        'description'           => 'descripción',
        'excerpt'               => 'extracto',
        'date'                  => 'fecha',
        'time'                  => 'hora',
        'subject'               => 'asunto',
        'message'               => 'mensaje',
        'id'                    => 'id',
        'programming_id'        => 'programación',
        'entity_id'             => 'entidad',
        'entity_name'           => 'entidad',
        'type_id'               => 'tipo de población',
        'type_population'       => 'tipo de población',
        'condition_id'          => 'condición',
        'condition_name'        => 'condición',
        'situation_id'          => 'situación',
        'situation_name'        => 'situación',
        '0_5_f'                 => 'femenino de 0 a 5 años',
        'age_0_5_f'             => 'femenino de 0 a 5 años',
        '0_5_m'                 => 'masculino de 0 a 5 años',
        'age_0_5_m'             => 'masculino de 0 a 5 años',
        '6_12_f'                => 'femenino de 6 a 12 años',
        'age_6_12_f'            => 'femenino de 6 a 12 años',
        '6_12_m'                => 'masculino de 6 a 12 años',
        'age_6_12_m'            => 'masculino de 6 a 12 años',
        '13_17_f'               => 'femenino de 13 a 17 años',
        'age_13_17_f'           => 'femenino de 13 a 17 años',
        '13_17_m'               => 'masculino de 13 a 17 años',
        'age_13_17_m'           => 'masculino de 13 a 17 años',
        '18_26_f'               => 'femenino de 18 a 26 años',
        'age_18_26_f'           => 'femenino de 18 a 26 años',
        '18_26_m'               => 'masculino de 18 a 26 años',
        'age_18_26_m'           => 'masculino de 18 a 26 años',
        '27_59_f'               => 'femenino de 27 a 59 años',
        'age_27_59_f'           => 'femenino de 27 a 59 años',
        '27_59_m'               => 'masculino de 27 a 59 años',
        'age_27_59_m'           => 'masculino de 27 a 59 años',
        '60_f'                  => 'femenino 60 años o más',
        'age_60_f'              => 'femenino 60 años o más',
        '60_m'                  => 'masculino 60 años o más',
        'age_60_m'              => 'masculino 60 años o más',
        'subtotal'              => 'subtotal',
        'created_at'            => 'created_at',
    ],
];